﻿var flag = 0;
var list_id = [];

$(document).ready(function () {
    $("#btn-add").on("click", function () {
        CheckCustomerID();

    });

    $('.onlynumber').keyup(function (e) {
        if (/\D/g.test(this.value)) {
            this.value = this.value.replace(/\D/g, '');
        }
    });

    function CheckCustomerID() {
        var cus_ids = $("#Customer_CustomerID").val();
        var cus_name = $("#Customer_CustomerName").val();
        var cus_outlet = $("#Customer_OutletId").val();

        if (cus_ids != "") {
            $.ajax({
                url: "/Customer/CheckCustomersExists/",
                method: "GET",
                data: { ids: cus_ids },
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response == true) {
                        // ID Exists/Available in Database
                        alert_notification_danger();
                    } else {
                        // ID Not Available in Database
                        // Customer Name & Outlet Not Null
                        if (cus_name != "" && cus_outlet != "") {
                            if (list_id.includes(cus_ids)) {
                                //ID Already used in Temporary Table
                                alert_notification_danger();
                            } else {
                                add_value();
                                HideBtnSubmit();
                            }
                        } else {
                            alert_notification();
                        }
                    }
                },
                error: function (response) {
                    alert_notification_danger();
                }
            });
        } else {
            alert_notification();
        };
    };

    function HideBtnSubmit() {
        if ($("#tbody-create tr").length > 0) {
            $("#btn-submit").attr('hidden', false)
        } else {
            $("#btn-submit").attr('hidden', true)
        };
    }

    function reset() {
        $("#Customer_CustomerID").val("");
        $("#Customer_CustomerName").val("");
        $("#Customer_Address").val("");
        $("#Customer_Code").val("");
        $("#Customer_StartDate").val("");
        $("#Customer_EndDate").val("");
        $("#Customer_OutletId").val("");

        $(".customerid-validation").addClass("d-none");
    };

    function add_value() {
        var _customerid = $("#Customer_CustomerID").val();
        var _customername = $("#Customer_CustomerName").val();
        var _address = $("#Customer_Address").val();
        var _code = $("#Customer_Code").val();
        var _startdate = $("#Customer_StartDate").val();
        var _endate = $("#Customer_EndDate").val();
        var _outletid = $("#Customer_OutletId").val();

        var _input_customid = '<input type="text" name="item[' + flag + '].CustomerID" class="form-control border-0 py-0" value="' + _customerid + '" readonly />';
        var _input_customername = '<input type="text" name="item[' + flag + '].CustomerName" class="form-control border-0 py-0" value="' + _customername + '" readonly />';
        var _input_address = '<input type="text" name="item[' + flag + '].Address" class="form-control border-0 py-0" value="' + _address + '" readonly />';
        var _input_code = '<input type="text" name="item[' + flag + '].Code" class="form-control border-0 py-0" value="' + _code + '" readonly />';
        var _input_startdate = '<input type="text" name="item[' + flag + '].StartDate" class="form-control border-0 py-0" value="' + _startdate + '" readonly />';
        var _input_enddate = '<input type="text" name="item[' + flag + '].EndDate" class="form-control border-0 py-0" value="' + _endate + '" readonly />';
        var _input_outletid = '<input type="text" name="item[' + flag + '].OutletID" class="form-control border-0 py-0" value="' + _outletid + '" readonly />';

        var _tr = '<tr class="text-center" id=' + flag + '><td>' + _input_customid + '</td>' +
            '</td><td>' + _input_customername + '</input>' +
            '</td> <td>' + _input_code + '</input>' +
            '</td><td>' + _input_startdate + '</input>' +
            '</td><td>' + _input_enddate + '</input>' +
            '</td><td>' + _input_address + '</input>' +
            '</td><td>' + _input_outletid + '</input>' +
            '</td></tr>'

        $("#tbody-create").append(_tr);
        $("#btn-save").removeClass("d-none");
        list_id.push(_customerid);
        reset();
        flag++;
    };

    function alert_notification() {
        var alert_component = '<div class="alert alert-warning alert-dismissible fade show" role="alert">' +
            '<strong>Attention !</strong> Customer ID, Customer Name and Outlet Can Not be Null.' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span></button></div>'

        $("#alert-message").append(alert_component);
        $(".alert").delay(3000).fadeOut(
            "normal",
            function () {
                $(this).remove();
            });
    }

    function alert_notification_danger() {
        var cus_id = $("#Customer_CustomerID").val();

        var alert_component_danger = '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
            '<strong>Warning !</strong> Customer ID ' + cus_id + " Already Used !" +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span></button></div>'

        $("#alert-message").append(alert_component_danger);
        $(".alert").delay(3000).fadeOut(
            "normal",
            function () {
                $(this).remove();
            });
    };
});