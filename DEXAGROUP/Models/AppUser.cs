﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DEXAGROUP.Models
{
    public class AppUser : IdentityUser
    {
        public string FullName { get; set; }
    }
}
